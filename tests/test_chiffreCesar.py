import unittest
from chiffreCesar import chiffrage_circulaire, chiffrage_nn_circulaire, dechiffrage_circulaire, dechiffrage_nn_circulaire, transf_Majusc, transf_Minusc

class TestEncodageFunction(unittest.TestCase):

    """
    on suppose ici que la cle (ou decalage) est de 13
    """

    def test_chiffrage_nn_circulaire(self):
        self.assertEqual(chiffrage_nn_circulaire("Test",13),"ar")

    def test_dechiffrage_nn_circulaire(self):
        self.assertEqual(dechiffrage_nn_circulaire("ar",13),"Test")

    def test_dechiffrage_circulaire(self):
        self.assertEqual(chiffrage_circulaire("Test",13),"Grfg")

    def test_chiffrage_circulaire(self):
        self.assertEqual(dechiffrage_circulaire("Grfg",13),"Test")

    """
    Les tests de transf_Majusc et transf_Minusc se font via ceux du chiffrage/dechiffrage circulaire
    car ils y sont utilisés
    """