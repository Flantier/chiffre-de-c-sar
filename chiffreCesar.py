import string

def chiffrage_nn_circulaire(ligne,cle):
    """
    Permet de chiffrer selon la methode du chiffre de Cesar une chaine de caractere rentree en parametre.
    Le decalage (ou cle) est aussi a rentrer en parametre. Non circulaire.
    tab_traduction : Permet de stocker la traduction de la chaine de caractere.
    taille : taille de la chaine de caractere.
    caractere_actuel : caractere dont on ressortira sa traduction lors du passage en boucle.
    return : retourne la traduction de la chaine de caractere.
    """
    tabTraduction = []
    taille = len(ligne)
    caractereActuel =""

    for c in range (0,taille):
        caractereActuel = chr(ord(ligne[c])+cle)
        tabTraduction.append(caractereActuel)

    return ''.join(tabTraduction)


def dechiffrage_nn_circulaire(ligne,cle):
    """
    Permet de dechiffrer selon la methode du chiffre de Cesar une chaine de caractere rentree en parametre.
    Le decalage (ou cle) est aussi a rentrer en parametre. Non circulaire.
    tab_traduction : Permet de stocker la traduction de la chaine de caractere.
    taille : taille de la chaine de caractere.
    caractere_actuel : caractere dont on ressortira sa traduction lors du passage en boucle.
    return : retourne la traduction de la chaine de caractere.
    """
    tabTraduction = []
    taille = len(ligne)
    caractereActuel =""

    for c in range (0,taille):
        caractereActuel = chr(ord(ligne[c])-cle)
        tabTraduction.append(caractereActuel)

    return ''.join(tabTraduction)


def dechiffrage_circulaire(ligne,cle):
    """
    Permet de dechiffrer selon la methode du chiffre de Cesar une chaine de caractere rentree en parametre.
    Le decalage (ou cle) est aussi a rentrer en parametre. Circulaire.
    tab_traduction : Permet de stocker la traduction de la chaine de caractere.
    taille : taille de la chaine de caractere.
    caractere_actuel : caractere dont on ressortira sa traduction lors du passage en boucle.
    return : retourne la traduction de la chaine de caractere.
    """
    tabPhraseFinale = []
    taille = len(ligne)
    caractereActuel =""

    for c in range (0,taille):
        caractereActuel = ligne[c]
        for i in range (0,26):
            if (caractereActuel == string.ascii_uppercase[i]):
                tabPhraseFinale.append(transf_Majusc(caractereActuel,cle,"-"))
            elif (caractereActuel == string.ascii_lowercase[i]):
                tabPhraseFinale.append(transf_Minusc(caractereActuel,cle,"-"))

    return ''.join(tabPhraseFinale)


def chiffrage_circulaire(ligne,cle):
    """
    Permet de chiffrer selon la methode du chiffre de Cesar une chaine de caractere rentree en parametre.
    Le decalage (ou cle) est aussi a rentrer en parametre. Circulaire.
    tab_traduction : Permet de stocker la traduction de la chaine de caractere.
    taille : taille de la chaine de caractere.
    caractere_actuel : caractere dont on ressortira sa traduction lors du passage en boucle.
    return : retourne la traduction de la chaine de caractere.
    """
    tabPhraseFinale = []
    taille = len(ligne)
    caractereActuel =""

    for c in range (0,taille):
        caractereActuel = ligne[c]
        for i in range (0,26):
            if (caractereActuel == string.ascii_uppercase[i]):
                tabPhraseFinale.append(transf_Majusc(caractereActuel,cle,"+"))
            elif (caractereActuel == string.ascii_lowercase[i]):
                tabPhraseFinale.append(transf_Minusc(caractereActuel,cle,"+"))

    return ''.join(tabPhraseFinale)


def transf_Majusc(caractere,cle,sens):
    """
    Transcrit les majuscule en fonction de s'il s'agit de chiffrage ou dechiffrage
    sens : permet de savoir si on chiffre ('+') ou déchiffre ('-')
    caractere : le caractere que l'on veut modifier
    cle : indique le deplacement necessaire au chiffrement
    return : renvoie le caractere traduit
    """
    if (sens == "-"):
        if ((ord(caractere)-cle) < 65):
            return chr(ord(caractere)- cle + 26)
        else:
            return chr(ord(caractere)- cle)
    elif (sens == "+"):
        if ((ord(caractere)+cle) > 90):
            return chr(ord(caractere)+ cle - 26)
        else:
            return chr(ord(caractere)+ cle)


def transf_Minusc(caractere,cle,sens):
    """
    Transcrit les minuscule en fonction de s'il s'agit de chiffrage ou dechiffrage
    sens : permet de savoir si on chiffre ('+') ou déchiffre ('-')
    caractere : le caractere que l'on veut modifier
    cle : indique le deplacement necessaire au chiffrement
    return : renvoie le caractere traduit
    """
    if (sens == "-"):
        if ((ord(caractere)-cle) < 97):
            return chr(ord(caractere)- cle + 26)
        else:
            return chr(ord(caractere)- cle)
    elif (sens == "+"):
        if ((ord(caractere)+cle) > 122):
            return chr(ord(caractere)+ cle - 26)
        else:
            return chr(ord(caractere)+ cle)