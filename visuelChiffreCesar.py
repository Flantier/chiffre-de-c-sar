from tkinter.messagebox import *
from tkinter import * 
from chiffreCesar import *


def validation_chiffrage():
    """
    chiffre la chaine de caractere si le bouton equivalent est appuye
    resultat : Entry ou l'on affichera le resultat du codage si possible
    entree_phrase : Entry ou se situe la phrase a coder
    entree_cle : Entry ou la cle est indiquee par l'utilisateur
    indication : Valeur du checkbox indiquant si circulaire ou non
    """
    resultat.delete(0,'end')
    test = StringVar()
    test.set('non')
    
    if (indication.get() == test.get()):
        try:
            resultat.insert(0,chiffrage_nn_circulaire(entree_phrase.get(),int(entree_cle.get())))
        
        except ValueError:
                
            resultat.insert(0,"!!!Erreur clé codage!!!")
    else:
        try:
            resultat.insert(0,chiffrage_circulaire(entree_phrase.get(),int(entree_cle.get())))
        
        except ValueError:
                
            resultat.insert(0,"!!!Erreur clé codage!!!")
        

def validation_dechiffrage():
    """
    dechiffre la chaine de caractere si le bouton equivalent est appuye
    resultat : Entry ou l'on affichera le resultat du codage si possible
    entree_phrase : Entry ou se situe la phrase a coder
    entree_cle : Entry ou la cle est indiquee par l'utilisateur
    indication : Valeur du checkbox indiquant si circulaire ou non
    """   
    resultat.delete(0,'end')
    test = StringVar()
    test.set('non')

    if (indication.get() == test.get()):
        try:
            resultat.insert(0,dechiffrage_nn_circulaire(entree_phrase.get(),int(entree_cle.get())))

        except ValueError:
                
            resultat.insert(0,"!!!Erreur clé codage!!!")
    else:
        try:
            resultat.insert(0,dechiffrage_circulaire(entree_phrase.get(),int(entree_cle.get())))

        except ValueError:
                
            resultat.insert(0,"!!!Erreur clé codage!!!")








"""
menu principal
"""
menu = Tk()
menu.title("Chiffre de Cesar")
menu.rowconfigure(5, weight=1)
menu.columnconfigure(2, weight=1)


"""
les 'file' ci-dessous peuvent etre des problemes si on ne 
lance pas le programme depuis le fichier ou il se situe
(probleme de chemin)
"""
icon = PhotoImage(file='cesar.png')
menu.iconphoto(False, icon) 

img = PhotoImage(file='chirac.png')
l = Label(menu, image = img)
l.grid(row=7, column=1)



"""
label pour enoncer l'entree de la cle de codage
"""
lbl_init_cle = Label(menu, text="Clé de codage souhaitée :")
lbl_init_cle.grid(column=0, row=0)
"""
entry de la cle de codage
"""
entree_cle = Entry(menu, textvariable="ex : 8")
entree_cle.grid(column=0, row=1)



"""
label pour enoncer le choix circulaire ou non
"""
lbl_init_cle = Label(menu, text="Boucle de la table ASCII :")
lbl_init_cle.grid(column=1, row=0)
"""
Checkbutton pour choix circulaire
"""
indication = StringVar()
choix_circulaire = Checkbutton(menu, text="Circulaire", variable=indication, onvalue='oui', offvalue='non')
choix_circulaire.grid(column=1, row=1)



"""
label pour enoncer l'entree de la chaine de caractere
"""
lbl_init = Label(menu, text="Phrase à modifier :")
lbl_init.grid(column=2, row=0)
"""
entry de la chaine de caractere
"""
entree_phrase = Entry(menu, textvariable="C'est ici qu'il faut écrire")
entree_phrase.grid(column=2, row=1)



"""
label ennoncant le choix
"""
lbl_choix = Label(menu, text="Choix de modification :")
lbl_choix.grid(column=1, row=4)
"""
bouton choix chiffrage
"""
bouton_chiffrage = Button(menu,text="Chichiffrer",command=validation_chiffrage)
bouton_chiffrage.grid(column=0, row=5)
"""
bouton choix dechiffrage
"""
bouton_dechiffrage = Button(menu,text="Déchichiffrer",command=validation_dechiffrage)
bouton_dechiffrage.grid(column=2, row=5)



"""
entry finale
"""
resultat = Entry(menu, textvariable="résultat")
resultat.grid(column=1, row=6)



menu.mainloop()